function BinarySearch(arr,input){
  var min_Arr = arr[0];
  for(i = 1; i < arr.length; i++){
    if(arr[i] < min_Arr){
      var temp = arr[i];
      arr[i] = min_Arr;
      min_Arr = temp;
    }
  }

  var min = 0;
  var max = arr.length;
  var mid = 0;
  while (min <= max){
    mid = parseInt((min + max) /2);
    if (arr[mid] == input){
       return mid;
    } else if (arr[mid] < input){
      min = mid + 1;
    } else {
      max = mid - 1;
    }
  }
  return -1
}
BinarySearch([2, 3, 5, 7], 3);