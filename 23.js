function subnet(arr, n) {
  arr = SortArray(arr);                                      
  if (n > arr.length || n <= 0) {                                               
    return []                                                                   
  }                                                                             
  if (n === arr.length) {                                                       
    return [arr]                                                    
  }                                                                             
  var res = []                                                              
  if (n === 1) {                                                                
    for (let i = 0; i < arr.length; i++) {                                      
      res.push([arr[i]])                                                      
    }                                                                           
    return res                                                                
  }                                                                             
  for (let i = 0; i < arr.length - n + 1; i++) {
    debugger;                               
    var firstItem = arr.slice(i, i + 1) ;
    var nextListItem = subnet(arr.slice(i + 1), n - 1);
    for (let j = 0; j < nextListItem.length; j++) {                                
      res.push(firstItem.concat(nextListItem[j]))                                     
    }                                                                           
  }                                                                             
  return res;
}

function SortArray(arr){
  for(i =0; i< arr.length; i++){
    for(j = i+1; j< arr.length; j++){
      if (arr[i] > arr[j]){
        var temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
      }
    }
  }
  return arr;
}

console.log(subnet([5,1,3,4,2], 3));
