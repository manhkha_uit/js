function CountVowels(input){
  var count = 0;
  var arr_vowels = ['a', 'e', 'i', 'o', 'u'];
  for (i = 0; i < arr_vowels.length; i++){
    for(j = 0; j < input.length; j++ ){
      if(arr_vowels[i] == input[j].toLowerCase()) {
        count++;
      }
    }
  }
  console.log(count);
}
CountVowels('The quick brown fox');